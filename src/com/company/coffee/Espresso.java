package com.company.coffee;

/**
 * 05.11.2020.
 *
 * @author Mars'el Serazetdinov (sermarsel@mail.ru).
 */
public class Espresso {

	static int caffeine = 5;
	private int price;

	public int getPrice() {
		return price;
	}

	public Espresso(int price) {
		this.price = price;
	}

	public Espresso() {
	}

	public int getCaffeine() {
		return caffeine;
	}

	public static void setCaffeine(int caffeine) {
		Espresso.caffeine = caffeine;
	}
}
