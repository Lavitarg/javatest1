package com.company.coffee;

/**
 * 05.11.2020.
 *
 * @author Mars'el Serazetdinov (sermarsel@mail.ru).
 */
public class Latte extends Espresso {

    public Latte(int price) {
        super(price);
    }
}
