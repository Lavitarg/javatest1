package com.company.coffee;

import java.util.Objects;

/**
 * 05.11.2020.
 *
 * @author Mars'el Serazetdinov (sermarsel@mail.ru).
 */
public class CoffeeTypesTest {

	public static void main(String[] args) {

		Espresso espresso = new Espresso();
		int price = espresso.getPrice();

		Latte latte = new Latte(4567);


		System.out.println(espresso.getClass());

		System.out.println(espresso instanceof Espresso);
		System.out.println(espresso instanceof Object);


		FlatWhite flatWhite = new FlatWhite();
		System.out.println(flatWhite instanceof Espresso);
		System.out.println(flatWhite instanceof FlatWhite);
		System.out.println(flatWhite instanceof Object);

		Ristretto ristretto = new Ristretto();

		Object[] objects = new Object[3];

		objects[0] = espresso;
		objects[1] = new String("h");
		objects[2] = ristretto;

		for (int i = 0; i < objects.length; i++) {
			if (objects[i] instanceof Espresso) {
				int x;
				System.out.println(((Espresso) objects[i]).getCaffeine());
			}
		}


	}
}
