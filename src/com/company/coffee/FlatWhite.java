package com.company.coffee;

/**
 * 05.11.2020.
 *
 * @author Mars'el Serazetdinov (sermarsel@mail.ru).
 */
public class FlatWhite extends Espresso {

	@Override
	public int getCaffeine() {
		return super.getCaffeine() * 2;
	}
}
