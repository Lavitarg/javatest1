package com.company.equalscontract;

import java.util.HashSet;

public class EqTest {

    public static void main(String[] args) {

        Cat cat1 = new Cat("Tom", "grey", 14);
        Cat cat2 = cat1;

//        System.out.println(cat1.equals(cat2));
//
//        System.out.println(cat1.hashCode() == cat2.hashCode());

        Cat cat3 = new Cat("Garfield", "ginger", 2);
        Cat cat4 = new Cat("Tom", "grey", 14);
        Cat cat5 = new Cat("Tom", "grey", 14);

        HashSet<Cat> cats = new HashSet<>();

        System.out.println(cats.add(cat3));
        System.out.println(cats.add(cat4));
        System.out.println(cats);



    }


}

class Cat {
    private String name;
    private String color;
    private int age;



    public Cat(String name, String color, Integer age) {
        this.name = name;
        this.color = color;
        this.age = age;
    }

    @Override
    public int hashCode() {
        return 1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return name;
    }
}
