package com.company.airconditioning;

public interface IkPortCompatible {

	String turnOn();
	String turnOff();
}
