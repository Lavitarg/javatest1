package com.company.airconditioning;

/**
 * 31.10.2020.
 *
 * @author Mars'el Serazetdinov (sermarsel@mail.ru).
 */
public class RemoteControl {


	public void changeTemperature(int temp, AirConditioner conditioner) {
		conditioner.setTemp(temp);
	}


	@Override
	public String toString() {
		return "RemoteControl{" +
				'}';
	}

	String turnSomethingOn(IkPortCompatible somethingWithIkPort){
		return somethingWithIkPort.turnOn();
	}

	String turnSomethingOff(IkPortCompatible somethingWithIkPort){
		return somethingWithIkPort.turnOff();
	}
}
