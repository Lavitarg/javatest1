package com.company.airconditioning;

/**
 * 31.10.2020.
 *
 * @author Mars'el Serazetdinov (sermarsel@mail.ru).
 */
public class AirConditioner implements IkPortCompatible {
	int temp;

	public int getTemp() {
		return temp;
	}

	public void setTemp(int temp) {
		this.temp = temp;
	}

	public AirConditioner() {
		this.temp = 20;
	}

	@Override
	public String toString() {
		return "AirConditioner{" +
				"temp=" + temp +
				'}';
	}

	@Override
	public String turnOn() {
		return "Conditioner turned On";
	}

	@Override
	public String turnOff() {
		return "Conditioner turned off";
	}
}
