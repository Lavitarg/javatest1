package com.company.airconditioning;

import com.company.space.Spaceship;

/**
 * 31.10.2020.
 *
 * @author Mars'el Serazetdinov (sermarsel@mail.ru).
 */
public class AirCondTest {

	public static void main(String[] args) {

		AirConditioner airConditioner1 = new AirConditionerCoffee();
		AirConditioner airConditioner2 = new AirConditionerWithMusic();
		printDescription(airConditioner1);
		printDescription(airConditioner2);

		Spaceship spaceship = new Spaceship("Normandy");

		RemoteControl remoteControl = new RemoteControl();

		System.out.println(remoteControl.turnSomethingOn(airConditioner1));


	}

	private static void printDescription(AirConditioner conditioner){
		System.out.println(conditioner.toString());
	}
}
