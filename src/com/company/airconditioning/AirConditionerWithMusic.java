package com.company.airconditioning;

/**
 * 31.10.2020.
 *
 * @author Mars'el Serazetdinov (sermarsel@mail.ru).
 */
public class AirConditionerWithMusic extends AirConditioner {

	String genre;


	public AirConditionerWithMusic() {
		super();
	}


	@Override
	public String toString() {
		return "AirConditionerWithMusic{" +
				"genre='" + genre + '\'' +
				", temp=" + temp +
				'}';
	}
}
