package com.company.airconditioning;

/**
 * 31.10.2020.
 *
 * @author Mars'el Serazetdinov (sermarsel@mail.ru).
 */
public class AirConditionerWithFlashlight extends AirConditioner {

	String light;


	public AirConditionerWithFlashlight() {
		super();
	}

	@Override
	public String toString() {
		return "AirConditionerWithFlashlight{" +
				"light='" + light + '\'' +
				", temp=" + temp +
				'}';
	}
}
