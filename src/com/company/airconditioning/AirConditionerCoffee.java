package com.company.airconditioning;

/**
 * 31.10.2020.
 *
 * @author Mars'el Serazetdinov (sermarsel@mail.ru).
 */
public class AirConditionerCoffee extends AirConditioner {

	public AirConditionerCoffee() {
		super();
	}

	@Override
	public String toString() {
		return "Conditioner with coffee";
	}
}
