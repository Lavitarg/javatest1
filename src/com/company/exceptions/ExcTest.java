package com.company.exceptions;

import java.io.FileNotFoundException;

public class ExcTest {

    public static void main(String[] args) {
        try {
            someRiskyMethod();
        } catch (IllegalArgumentException | ArithmeticException ie) {
            System.out.println("IAE or AE it is");
        } catch (NullPointerException npe) {
            System.out.println(npe.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        finally {
            System.out.println("And this is an finally block, bye!");
        }

    }

    private static void someRiskyMethod() {
        //do something
        throw new NullPointerException();

    }


    private static void someCheckedRiskyMethod() throws FileNotFoundException {
        throw new FileNotFoundException();
    }

}
