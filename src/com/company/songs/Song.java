package com.company.songs;

public class Song implements Comparable<Song> {

    private String title;
    private String author;
    private String album;
    private String duration;

    public Song(String title, String author, String album, String duration) {
        this.title = title;
        this.author = author;
        this.album = album;
        this.duration = duration;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return title + " - " + author + "; " + album + ", " + duration;
    }

    @Override
    public int compareTo(Song o) {
        return this.title.compareTo(o.getTitle());
    }

    @Override
    public boolean equals(Object obj) {

        Song anotherSong = (Song) obj;
        return this.getTitle().equals(anotherSong.getTitle());
    }

    @Override
    public int hashCode() {
        return title.hashCode();
    }
}
