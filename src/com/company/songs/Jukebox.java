package com.company.songs;


import com.company.animals.Animal;

import java.util.*;

public class Jukebox {



    ArrayList<String> strSongList = new ArrayList<>();
    ArrayList<Song> songList = new ArrayList<>();

    public static void main(String[] args) {
        Jukebox jukebox = new Jukebox();
//        jukebox.run1();
//        jukebox.run2();
        jukebox.run3();

    }

    private void run3() {

        String songsRawData = getSongsRawDataFull();
        String[] rawStrings = songsRawData.split("\n");

        for (String rawString : rawStrings) {
            String[] split = rawString.split("/");
            songList.add(new Song(split[0], split[1], split[2], split[3]));
        }




        Collections.sort(songList);
        songList.stream().filter(song -> song!= null);

        System.out.println("songlist by title");
        System.out.println(songList);
        HashSet<Song> songHashSet = new HashSet<>(songList);
        System.out.println("pure set");
        System.out.println(songHashSet);
        System.out.println("tree set by title (compareTo at E)");
        TreeSet<Song> songTreeSet = new TreeSet<>(songHashSet);
        System.out.println(songTreeSet);



        TreeSet<Song> treeSetWithComparatorByAuthor = new TreeSet<>(new Comparator<Song>() {
            @Override
            public int compare(Song o1, Song o2) {
                return o1.getAlbum().compareTo(o2.getAlbum());
            }
        });
        treeSetWithComparatorByAuthor.addAll(songTreeSet);
        System.out.println("tree set with comparator by author");
        System.out.println(treeSetWithComparatorByAuthor);

    }


    private void run2() {

        String songsRawData = getSongsRawDataExtended();
        String[] rawStrings = songsRawData.split("\n");

        for (String rawString : rawStrings) {
            String[] split = rawString.split("/");
            songList.add(new Song(split[0], split[1], split[2], split[3]));
        }

        Collections.sort(songList);
        System.out.println("songlist by title");
        System.out.println(songList);
        Collections.sort(songList, new Comparator<Song>() {
            @Override
            public int compare(Song o1, Song o2) {
                return 0;
            }
        });
        System.out.println("songlist by author");
        System.out.println(songList);

    }

    private void run1() {

        String songsRawData = getSongsRawData();
        String[] rawStrings = songsRawData.split("\n");

        for (String rawString : rawStrings) {
            strSongList.add(rawString.split("/")[0]);
        }

        Collections.sort(strSongList);
        System.out.println(strSongList);


    }


    private String getSongsRawData(){
        return "Smells Like Teen Spirit/Nirvana \n" +
                "R U Mine?/Arctic Monkeys \n" +
                "Seven Nation Army/White Stripes \n" +
                "The Only/Static X \n" +
                "Bleeding Me/Metallica \n";
    }
    private String getSongsRawDataExtended(){
        return "Smells Like Teen Spirit/Nirvana/Nevermind/4:59 \n" +
                "R U Mine?/Arctic Monkeys/AM/3:20 \n" +
                "Seven Nation Army/White Stripes/Elephant/3:55 \n" +
                "The Only/Static X/Shadow Zone/2:51 \n" +
                "Bleeding Me/Metallica/Load/8:17 \n";
    }
    private String getSongsRawDataFull(){
        return "Smells Like Teen Spirit/Nirvana/Nevermind/4:59 \n" +
                "R U Mine?/Arctic Monkeys/AM/3:20 \n" +
                "Seven Nation Army/White Stripes/Elephant/3:55 \n" +
                "The Only/Static X/Shadow Zone/2:51 \n" +
                "Bleeding Me/Metallica/Load/8:17 \n" +
                "Smells Like Teen Spirit/Nirvana/Nevermind/4:59 \n" +
                "Smells Like Teen Spirit/Nirvana/Nevermind/4:59 \n" +
                "Smells Like Teen Spirit/Nirvana/Nevermind/4:59 \n" +
                "Smells Like Teen Spirit/Nirvana/Nevermind/4:59 \n" +
                "Seven Nation Army/White Stripes/Elephant/3:55 \n";
    }



    private void m1(ArrayList<Animal> entryList){

    }

    private <T extends Animal> void m2(ArrayList<T> entryList){

    }

    class SongByAuthorComparator implements Comparator<Song> {
        @Override
        public int compare(Song o1, Song o2) {
            return o1.getAuthor().compareTo(o2.getAuthor());
        }

    }


}

