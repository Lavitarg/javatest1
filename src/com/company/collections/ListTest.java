package com.company.collections;

import com.company.shapes.ColoredBox;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class ListTest {

	public ListTest() {
		super();
	}

	public static void main(String[] args) {


		ArrayList<String> boxesList = new ArrayList<>();

		boxesList.add("vaska");



		String s = boxesList.get(0);

		int size = boxesList.size();
		boolean empty = boxesList.isEmpty();

		boolean isVaskaPresented = boxesList.contains("vaska");
		int indexOfVaska = boxesList.indexOf("vasks");

		boxesList.remove("vaska");

		boxesList.clear();
		boxesList.trimToSize();
		boxesList.ensureCapacity(10);


		ArrayList<ColoredBox> coloredBoxes = new ArrayList<>();

		coloredBoxes.add(new ColoredBox("red", 5));
		coloredBoxes.add(new ColoredBox("blue", 10));

		for(ColoredBox coloredBox :coloredBoxes){
			coloredBox.setColor("white");
		}

		ColoredBox coloredBox = coloredBoxes.get(0);
		coloredBox.setColor("grey");

		System.out.println(coloredBoxes.get(0).getColor());

		TestClassTwo myTestClass2 = new TestClassTwo();

		TestClassTwo two = myTestClass2;
		InterfaceOne oneInterface = myTestClass2;
		InterfaceTwo twoInterface = myTestClass2;
		TestClass testClass = myTestClass2;
		Object object = myTestClass2;

		//some logic here

		Set<Integer> mySet= new HashSet<>();

		mySet.add(Integer.parseInt("10"));
		mySet.add(Integer.parseInt("10"));


	}
}
