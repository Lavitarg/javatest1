package com.company.collections;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapTest {

    public static void main(String[] args) {

        HashMap<String, String> stringStringHashMap = new HashMap<>();

        stringStringHashMap.put("vaska", "ginger");
        stringStringHashMap.put("vaska", "red");
        stringStringHashMap.put("tom", "grey");
        stringStringHashMap.put("felix", "black");
        String vaska = stringStringHashMap.get("vaska");
        System.out.println(stringStringHashMap);

        stringStringHashMap.keySet().forEach(System.out::println);
        System.out.println("____________________________");
        Set<Map.Entry<String, String>> entries = stringStringHashMap.entrySet();
        for (Map.Entry<String, String> entry : entries) {
            System.out.println(entry);
        }

        stringStringHashMap.values().forEach(System.out::println);

    }
}
