package com.company.animals;

import com.company.animals.Animal;

public abstract class Canine extends Animal {

	abstract void bark();

}
