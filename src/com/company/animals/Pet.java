package com.company.animals;

public interface Pet {

	void beFriendly();

	void play();

}
