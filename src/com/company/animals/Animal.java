package com.company.animals;

public abstract class Animal {

	abstract void toBreathe();

	void die(){
		System.out.println(this.getClass()+" is dead(");
	}
}
