package com.company.animals;


import java.util.ArrayList;
import java.util.List;

public class Dog extends Canine implements Pet {

	@Override
	void bark() {
		System.out.println("Bark!");
	}

	@Override
	void toBreathe() {
		System.out.println("Dog breathes heavily");
	}

	@Override
	public void beFriendly() {

	}

	@Override
	public void play() {

	}
}
