package com.company.animals;

public class SnakeCage {

	public Snake snake;

	public Snake getSnake() {
		return snake;
	}

	public void setSnake(Snake snake) {
		this.snake = snake;
	}
}
