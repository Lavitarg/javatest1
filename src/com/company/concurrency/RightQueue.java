package com.company.concurrency;

public class RightQueue {

    public static void main(String[] args) {
        new RightQueue().go();
    }

    private void go() {

        MyQ myQ = new MyQ();

        new Producer(myQ);
        new Consumer(myQ);

    }


    class Producer implements Runnable {

        public MyQ queue;

        public Producer(MyQ queue) {
            this.queue = queue;
            new Thread(this, "Поставщик").start();
        }

        @Override
        public void run() {
            int i = 0;

            while (true) {
                try {
                    queue.put(i++);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class Consumer implements Runnable {

        public MyQ queue;

        public Consumer(MyQ queue) {
            this.queue = queue;
            new Thread(this, "Потребитель").start();
        }

        @Override
        public void run() {
            while (true) {
                try {
                    queue.get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class MyQ {
        int n;

        boolean flag = true;

        synchronized void put(int n) throws InterruptedException {

            while (!flag) {
                this.wait();
            }
            this.n = n;
            System.out.println("Отправили: " + n);
            flag = false;
            this.notifyAll();

        }

        synchronized int get() throws InterruptedException {

            while (flag){
                this.wait();
            }
            System.out.println("Получили: " + n);
            flag = true;
            this.notifyAll();
            return n;
        }
    }
}
