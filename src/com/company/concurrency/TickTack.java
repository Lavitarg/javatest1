package com.company.concurrency;

public class TickTack {

    public static void main(String[] args) {
        Clock clock = new Clock();

        Ticker ticker = new Ticker(clock);
        Tacker tacker = new Tacker(clock);

        new Thread(ticker::tick).start();
        new Thread(tacker::tack).start();
    }

    private static class Clock {

        public boolean isTick;


        public synchronized void tick() throws InterruptedException {

            while (isTick) {
                wait();
            }

            System.out.println("Tick..");
            isTick = true;
            Thread.sleep(1000);
            notify();
        }

        public synchronized void tack() throws InterruptedException {
            while (!isTick) {
                wait();
            }

            System.out.println("Tack!");
            isTick = false;
            Thread.sleep(1000);
            notify();
        }
    }

    private static class Ticker {

        private final Clock clock;

        public Ticker(Clock clock) {
            this.clock = clock;
        }

        public void tick() {
            while (true) {
                try {
                    clock.tick();
                } catch (InterruptedException e) {
                    System.out.println("Interrupted exception");
                }
            }
        }
    }

    private static class Tacker {

        private final Clock clock;

        public Tacker(Clock clock) {
            this.clock = clock;
        }

        public void tack() {
            while (true) {
                try {
                    clock.tack();
                } catch (InterruptedException e) {
                    System.out.println("Interrupted exception");
                }
            }
        }

    }
}
