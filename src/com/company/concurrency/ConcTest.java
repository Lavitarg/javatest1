package com.company.concurrency;

import java.util.Collections;

public class ConcTest {

    public volatile static int x = 10;

    public static void main(String[] args) {

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Collections.sync

        System.out.println("Hello from main thread, starting programm");
        Thread thread1 = new Thread(() -> System.out.println(Thread.currentThread().getName()));
        thread1.start();
        try {
            thread1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Finishing main");

    }
}

class MyRunnable implements Runnable{

    private final String name;

    public MyRunnable(String name) {
        this.name = name;
    }

    @Override
    public void run() {

        System.out.println("Started thread " + name + "!");

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Finished thread " + name + "!");

    }
}
class MyThread extends Thread {

    private final String name;

    public MyThread(String name) {
        this.name = name;
    }

    @Override
    public void run() {

        System.out.println("Started thread " + name + "!");

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Finished thread " + name + "!");

    }
}


