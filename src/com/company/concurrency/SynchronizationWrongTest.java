package com.company.concurrency;

public class SynchronizationWrongTest {

    public static void main(String[] args) {

        new SynchronizationWrongTest().go();

    }

    private void go() {
        Printer printer = new Printer();

        PrinterUser user1 = new PrinterUser("Да здравствует", printer);
        PrinterUser user2 = new PrinterUser("великая", printer);
        PrinterUser user3 = new PrinterUser("синхронизация!", printer);

        try {

            user1.t.join();
            user2.t.join();
            user3.t.join();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Выполнение завершено");
    }

    class Printer {

        public void print(String msg){
            System.out.println("***" + msg);
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                System.out.println("Printer was interrupted");
            }
            System.out.println("###");
        }
    }

    class PrinterUser implements Runnable{

        public String message;
        public Printer printer;
        public Thread t;

        public PrinterUser(String message, Printer printer) {
            this.message = message;
            this.printer = printer;
            t = new Thread(this);
            t.start();
        }

        @Override
        public void run() {
            printer.print(message);
        }
    }
}
