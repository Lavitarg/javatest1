package com.company.space;

public enum ShipType {

	BATTLESHIP("Линкор", true);

	public String getTitle() {
		return title;
	}

	private final String title;
	private long id;
	private final boolean isArmed;


	private ShipType(String title, boolean isArmed) {
		this.title = title;
		this.isArmed = isArmed;
	}
}
