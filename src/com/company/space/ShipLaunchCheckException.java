package com.company.space;

import java.io.File;
import java.io.FileNotFoundException;

public class ShipLaunchCheckException extends FileNotFoundException {
    public ShipLaunchCheckException(String message) {
        super(message);
    }
}
