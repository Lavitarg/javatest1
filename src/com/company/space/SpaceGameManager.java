package com.company.space;


import java.util.Date;

public class SpaceGameManager {

	public static void main(String[] args) {

		 Date date = new Date("Tue Feb 02 08:52:25 GMT 2021");
		System.out.println(date);

		Cannon laser = new LaserCannon(5);
		System.out.println(laser.toString());
		Spaceship normandy = new Spaceship("Normandy");
		normandy.setCurrentWeapon(laser);
	}

	private void simulateBattle() {
		Spaceship ship1 = new Spaceship("Falcon");
		Cannon laserGun = new Cannon(5, "laser");
		ship1.setCurrentWeapon(laserGun);
		System.out.println(ship1);

		Spaceship ship2 = new Spaceship("Enterprise");
		Cannon railGun = new Cannon(10, "rail");
		ship2.setCurrentWeapon(railGun);

		ship2.takeDmg(ship1.getCurrentWeapon().getDmg());

		System.out.println(ship2.toString());
	}

}
