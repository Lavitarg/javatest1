package com.company.space;

/**
 * 24.10.2020.
 *
 * @author Mars'el Serazetdinov (sermarsel@mail.ru).
 */
public class Cannon {

	private int dmg;
	private String type;

	public Cannon(int dmg, String type) {
		this.dmg = dmg;
		this.type = type;
	}


	public Cannon() {
	}

	public int getDmg() {
		return dmg;
	}

	public void setDmg(int dmg) {
		this.dmg = dmg;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
