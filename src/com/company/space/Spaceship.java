package com.company.space;


import com.company.reflection.MyAnnotation;

public class Spaceship {

	@MyAnnotation(name = "My annotation parametr")
	private String name;
	private int durability;
	private boolean isAlive;
	private Cannon currentWeapon;
	private int fuel;


	public Spaceship(String name, int durability, boolean isAlive, Cannon currentWeapon, int fuel, ShipType type) {
		this.name = name;
		this.durability = durability;
		this.isAlive = isAlive;
		this.currentWeapon = currentWeapon;
		this.fuel = fuel;
		this.type = type;
	}

	public int getFuel() {
		return fuel;
	}

	public void setFuel(int fuel) {
		this.fuel = fuel;
	}

	public ShipType getType() {
		return type;
	}

	public void setType(ShipType type) {
		this.type = type;
	}

	private ShipType type;

	public Spaceship(String name) {
		this.name = name;
		this.durability = 20;
		this.isAlive = true;
	}

	public void takeDmg(int dmg) {
		System.out.println("Ship " + this.name + " has received " + dmg + " amount of dmg");
		this.durability -= dmg;
		if (this.durability <= 0) {
			this.isAlive = false;
			System.out.println("Ship " + this.name + " has been destroyed. Press F.");
		}
	}

	@Override
	public String toString() {
		return "Spaceship{" +
				"name='" + name + '\'' +
				", durability=" + durability +
				", isAlive=" + isAlive +
				", currentWeapon=" + currentWeapon +
				'}';
	}

	public String getName() {
		return this.name;
	}

	public int getDurability() {
		return durability;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDurability(int durability) {
		this.durability = durability;
	}

	public boolean isAlive() {
		return isAlive;
	}

	public void setAlive(boolean alive) {
		isAlive = alive;
	}

	public Cannon getCurrentWeapon() {
		return currentWeapon;
	}

	public void setCurrentWeapon(Cannon currentWeapon) {
		this.currentWeapon = currentWeapon;
	}

}