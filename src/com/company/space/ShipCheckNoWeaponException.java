package com.company.space;

public class ShipCheckNoWeaponException extends RuntimeException {


    public ShipCheckNoWeaponException(String message) {
        super(message);
    }
}
