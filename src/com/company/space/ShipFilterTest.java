package com.company.space;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ShipFilterTest {


    public static void main(String[] args) {

        List<Spaceship> shipList = new ArrayList<>();
        Class<Void> voidClass = Void.class;
        shipList.add(new Spaceship("Rocket", 100, true, null, 0, ShipType.BATTLESHIP));
        shipList.add(new Spaceship("Cutter", 50, true, new Cannon(), 0, ShipType.BATTLESHIP));

        System.out.println(Arrays.stream(new Integer[]{1, 2, 3, 4, 5})
                .peek(integer -> System.out.println(integer))
                .filter(integer -> integer % 2 == 0)
                .peek(integer -> System.out.println(integer))
                .collect(Collectors.toList()));



        Map<String, Spaceship> collect = shipList.stream()
                .filter(spaceship -> spaceship.getCurrentWeapon() == null)
                .collect(Collectors.toMap(spaceship -> spaceship.getName(), spaceship -> spaceship));
        System.out.println(collect);

    }
}
