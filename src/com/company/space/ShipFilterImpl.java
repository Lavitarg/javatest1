package com.company.space;

public class ShipFilterImpl implements ShipFilter {
    @Override
    public boolean filterShip(Spaceship spaceship) {
        return spaceship.getCurrentWeapon() == null;
    }
}
