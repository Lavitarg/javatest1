package com.company.space;

import java.io.FileNotFoundException;
import java.util.Objects;

public class SpaceshipHangar {

    public static void main(String[] args) {

        Spaceship spaceship = new Spaceship("Normandy", 10, true, new Cannon(10, "Laser"), 100, ShipType.BATTLESHIP);

        try {
        }catch (NullPointerException npe){

        }
        checkShipBeforeLaunch(spaceship);

    }

    public static boolean checkShipBeforeLaunch(Spaceship ship){

        if (ship == null ) {
            throw new NullPointerException("Ship is null!!111");
        }

        if (ship.getFuel() <= 0) {
            return false;
        }

        if( ship.getCurrentWeapon() == null ){
            throw new ShipCheckNoWeaponException("Ship named " + ship.getName() + " isn't armed!");
        }

        if(ship.getCurrentWeapon().getDmg() <= 0 ){
            return false;
        }

        if(ship.getDurability() <= 0 ){
            return false;
        }




        return true;


    }




    private static boolean checkDurability(int durability) {
        if (durability <= 0) {
            return true;
        }
        return false;
    }

    private static boolean checkWeapon(Spaceship ship) throws ShipLaunchCheckException {
        if(ship.getCurrentWeapon() == null){
            throw new ShipLaunchCheckException("Ship named " + ship.getName() + " isn't armed!");
        }
        return true;
    }

    private static boolean checkFuel(int fuel) {
        if (fuel <= 0) return false;
        return true;
    }

    private static boolean checkNullabilityOfShip(Spaceship ship) {
        if(Objects.isNull(ship)){
            throw new NullPointerException("Ship is null!");
        }
        return true;
    }

}
