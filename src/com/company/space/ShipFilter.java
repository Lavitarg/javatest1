package com.company.space;

public interface ShipFilter {

    boolean filterShip(Spaceship spaceship);
}
