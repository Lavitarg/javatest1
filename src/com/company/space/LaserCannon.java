package com.company.space;

/**
 * 24.10.2020.
 *
 * @author Mars'el Serazetdinov (sermarsel@mail.ru).
 */
public class LaserCannon extends Cannon {


	public LaserCannon(int dmg) {
		super(dmg, "laser");
	}
}
