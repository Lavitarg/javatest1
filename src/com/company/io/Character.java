package com.company.io;

import java.io.Serializable;

public class Character implements Serializable {

    private String name;
    private transient int healthPoints;
    private Weapon currentWeapon;

    public Character(String name, int healthPoints, Weapon currentWeapon) {
        this.name = name;
        this.healthPoints = healthPoints;
        this.currentWeapon = currentWeapon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealthPoints() {
        return healthPoints;
    }

    public void setHealthPoints(int healthPoints) {
        this.healthPoints = healthPoints;
    }

    public Weapon getCurrentWeapon() {
        return currentWeapon;
    }

    public void setCurrentWeapon(Weapon currentWeapon) {
        this.currentWeapon = currentWeapon;
    }

    @Override
    public String toString() {
        return "Character{" +
                "name='" + name + '\'' +
                ", healthPoints=" + healthPoints +
                ", currentWeapon=" + currentWeapon +
                '}';
    }
}
