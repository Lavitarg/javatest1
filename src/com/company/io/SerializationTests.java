package com.company.io;

import java.io.*;

public class SerializationTests {

    public static void main(String[] args) {
        SerializationTests serializationTests = new SerializationTests();
//        serializationTests.serializeSmth();
        serializationTests.deserializeSmth();
    }

    private void deserializeSmth() {

        try{
            FileInputStream fileInputStream = new FileInputStream("GameSave.ser");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            Object object = objectInputStream.readObject();
            Character elf = (Character) object;
            object = objectInputStream.readObject();
            Character orc = (Character) object;

            System.out.println(elf);
            System.out.println(orc);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void serializeSmth() {

        Character elf = new Character("Galadriel", 23, new Weapon("Longbow", 23));
        Character orc = new Character("Grokh", 23, new Weapon("Mace", 45));

        try {
            FileOutputStream fileOutputStream = new FileOutputStream("GameSave.ser");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(elf);
            objectOutputStream.writeObject(orc);

            objectOutputStream.close();



        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
