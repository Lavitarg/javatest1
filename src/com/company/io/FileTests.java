package com.company.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileTests {

    public static void main(String[] args) {

        new FileTests().go();
    }

    private void go() {

        Character elf = new Character("Galadriel", 23, new Weapon("Longbow", 23));
        Character orc = new Character("Grokh", 23, new Weapon("Mace", 45));

        try {
            File file = new File("CharacterFile.txt");


            FileWriter fileWriter = new FileWriter("CharacterFile.txt", true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write("new string");
            bufferedWriter.close();


        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
