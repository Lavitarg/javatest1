package com.company.shapes;

public class Box {

	private int width;
	private int height;
	private int length;

	public Box(int width, int height, int length) {

		this.width = width;
		this.height = height;
		this.length = length;
		System.out.println("Called all-args constructor from class " + this.getClass());

	}

	public Box() {
		System.out.println("Called no-args constructor from class " + this.getClass());
	}

	public Box(int size) {
		this(size, size, size);
		System.out.println("Called cube constructor with 1 arg from class " + this.getClass());
	}

	public int getVolume() {
		return width * height * length;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	@Override
	public String toString() {
		return "This is sample box with dimensions: " +
				"width=" + width +
				", height=" + height +
				", length=" + length +
				'.';
	}
}
