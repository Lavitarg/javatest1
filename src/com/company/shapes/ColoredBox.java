package com.company.shapes;

public class ColoredBox extends Box {

	protected String color;

	public ColoredBox(String color, int size) {
		super(size);
		this.color = color;
	}

	public static ColoredBox createSmallColoredBox(String color) {
		return new ColoredBox(color, 5);
	}

	public static ColoredBox createHugeColoredBox(String color) {
		return new ColoredBox(color, 55);
	}

	@Override
	public String toString() {

		return super.toString() + " And also it has " + color + " color!";
	}

	public ColoredBox() {
		this("black", 10);
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
}
