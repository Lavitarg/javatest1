package com.company.graphs;

public interface GraphService {

    String breadthFirst(boolean[][] adjacencyMatrix, int startIndex);

    boolean checkBrackets(String s);
}
