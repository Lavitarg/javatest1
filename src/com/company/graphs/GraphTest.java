package com.company.graphs;

public class GraphTest {

    public static void main(String[] args) {
//        GraphService graphService = new GraphServiceImplWithoutQueue();
//        System.out.println(graphService.breadthFirst(parseStringToBooleanMatrix(1), 1));
    }


    private static boolean[][] parseStringToBooleanMatrix(int number) {

        if(number == 1){

            String matrix = "0, 1, 0, \n" +
                    "1, 0, 1, \n" +
                    "0, 1, 0";

            String[] split = matrix.split("\n");

            boolean[][] result = new boolean[split.length][split.length];

            for (int i = 0; i < split.length; i++) {
                String s = split[i];
                String[] rowSplitResult = s.split(", ");
                for (int j = 0; j < rowSplitResult.length; j++) {
                    result[i][j] = rowSplitResult[j].equals("1");
                }
            }

            return result;
        }

        String matrix = "0, 1, 1, 0, 0, 0, 0, 0, 0, 0, \n" +
                "1, 0, 1, 1, 0, 0, 0, 0, 0, 0, \n" +
                "1, 1, 0, 0, 0, 0, 0, 0, 0, 1, \n" +
                "0, 1, 0, 0, 1, 0, 1, 0, 1, 0, \n" +
                "0, 0, 0, 1, 0, 1, 0, 0, 0, 0, \n" +
                "0, 0, 0, 0, 1, 0, 1, 0, 0, 0, \n" +
                "0, 0, 0, 1, 0, 1, 0, 1, 0, 0, \n" +
                "0, 0, 0, 0, 0, 0, 1, 0, 0, 0, \n" +
                "0, 0, 0, 1, 0, 0, 0, 0, 0, 1, \n" +
                "0, 0, 1, 0, 0, 0, 0, 0, 1, 0";

        String[] split = matrix.split("\n");

        boolean[][] result = new boolean[split.length][split.length];

        for (int i = 0; i < split.length; i++) {
            String s = split[i];
            String[] rowSplitResult = s.split(", ");
            for (int j = 0; j < rowSplitResult.length; j++) {
                result[i][j] = rowSplitResult[j].equals("1");
            }
        }

        return result;


    }
}
