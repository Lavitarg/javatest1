package com.company.graphs;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashSet;

public class GraphServiceImpl implements GraphService {

    ArrayDeque<Integer> queue;
    ArrayList<String> row;
    HashSet<Integer> set;


    @Override
    public boolean checkBrackets(String s) {

        return  false;
    }

    private boolean isBracketsMatch(Character pop, char currentChar) {

        switch (pop){
            case '(':
                return currentChar == ')';
            case '[':
                return currentChar == ']';
            case '{':
                return currentChar == '}';
        }
        return false;
    }

    @Override
    public String breadthFirst(boolean[][] adjacencyMatrix, int startIndex) {

        row = new ArrayList<>();
        queue = new ArrayDeque<>();
        set = new HashSet<>();
        set.add(startIndex);

        goBreadth(adjacencyMatrix, startIndex);

        return row.toString();
    }

    private String pasrseListOfIntsToCommaSeparatedString(ArrayList<String> row) {

        return null;
    }

    private void goBreadth(boolean[][] adjacencyMatrix, Integer startIndex) {

        for (int j = 0; j < adjacencyMatrix.length; j++) {
            if (!adjacencyMatrix[startIndex][j]) {
                continue;
            }
            if (!queue.contains(j) && !set.contains(j)) {
                queue.addLast(j);
                set.add(j);
            }
        }

        row.add(startIndex.toString());
        if (queue.isEmpty()) {
            return;
        }
        goBreadth(adjacencyMatrix, queue.poll());
    }
}
